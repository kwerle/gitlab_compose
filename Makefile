init:
	@mkdir -p ./gitlab/data
	@mkdir -p ./gitlab/logs
	@mkdir -p ./gitlab/config
	@mkdir -p ./gitlab-runner/config

start: init
	docker-compose up -d

stop:
	docker-compose down

runner-token = $(shell docker-compose exec web gitlab-rails runner "puts ApplicationSetting.last.runners_registration_token")

register-runner:
	docker-compose exec web gitlab-ctl reconfigure
	docker-compose run runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "http://web:8989/" \
  --registration-token "${runner-token}" \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
